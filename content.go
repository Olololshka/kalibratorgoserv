package main

import (
	"net/http"
	"html/template"
	"fmt"
	"log"
	"github.com/gorilla/mux"
)

const (
	SITE_HOME = "www"
	TEMPLATES_DIR = "templates"
	
	HEADER_TEMPLATE_NAME = "header.html"
	FOOTER_TEMPLATE_NAME = "footer.html"
)

func patchPath(name1 string, names ...string) []string {
	
	templates_path := GetWorkDir() + "/" + SITE_HOME + "/" + TEMPLATES_DIR
	
	res := make([]string, len(names) + 1)
	
	res[0] = templates_path + "/" + name1
	for i, na := range names {
		res[i + 1] = templates_path + "/" + na
	}
	
	return res
}

func PrepareTemplate(file1 string, files ...string) []string {
	otherFiles := make([]string, len(files) + 2)
	otherFiles[0] = HEADER_TEMPLATE_NAME
	otherFiles[1] = FOOTER_TEMPLATE_NAME
	for i, f := range files {
		otherFiles[i + 2] = f
	}
	
	return patchPath(file1, otherFiles...)
}

func Index(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles(PrepareTemplate("index.html")...)
                
    if err != nil {
    	fmt.Fprintf(w, "Error %s", err.Error())
    	return
    }
    
    if err := t.ExecuteTemplate(w, "index", nil); err != nil {
    	log.Println(err.Error())
    }
}

func ExampleIndex(w http.ResponseWriter, r *http.Request) {
	ServeFile(w, r, "example/index.html")
}

func examplesHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	page := vars["page"]
	
	ServeFile(w, r, fmt.Sprintf("example/%s", page))
}

func RedirectExampleHome(w http.ResponseWriter, r *http.Request) {
	log.Printf("redirecting to example index page")
	http.Redirect(w, r, "example/index.html", http.StatusFound)
}

func ServeFile(w http.ResponseWriter, r *http.Request, filename string) {
	log.Printf("Static file request: %s", filename)
	http.ServeFile(w, r, fmt.Sprintf("%s/%s/%s", GetWorkDir(), SITE_HOME, filename))
}

func Static(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	static_res := vars["static_res"]
	file := vars["file"]

	ServeFile(w, r, fmt.Sprintf("%s/%s", static_res, file))
}

func JS_Subfolder(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	foldername := vars["foldername"]
	file := vars["file"]

	ServeFile(w, r, fmt.Sprintf("js/%s/%s", foldername, file))
}

func RegisterContent(router *mux.Router) {
	router.HandleFunc("/", Index)
	
	router.HandleFunc("/example", RedirectExampleHome)
	router.HandleFunc("/example/", ExampleIndex)
	router.HandleFunc("/{static_res}/{file}", Static)
	router.HandleFunc("/js/{foldername}/{file}", JS_Subfolder)
}