package main

import (
	"os"
	"log"
	"errors"
	"encoding/json"
)

const (
	DEFAULT_CONFIG = 
`{
	"_port_description" : "Specify port for web server",
	"Port" : 8080
}`
)

type Configuration struct {
	Port int
}

var configuration *Configuration


func ReadSettings(configfilename string) error {
	file, err := os.Open(configfilename)
	if err != nil {
		log.Println("Configuration file open error, trying to create new one")
		
		file, err := os.Create(configfilename)
		if err != nil {
			return err
		}
		
		n, err := file.WriteString(DEFAULT_CONFIG)
		if n != len(DEFAULT_CONFIG) || err != nil {
			file.Close()
			return err
		}
		file.Close()
		return ReadSettings(configfilename)
	}
	
	decoder := json.NewDecoder(file)
	configuration = &Configuration{}
	err = decoder.Decode(configuration)
	if err != nil {
		// parce error
		file.Close()
		return err
	}
	file.Close()
	
	// validation
	if configuration.Port < 1 || configuration.Port > 65535 {
		return errors.New("Invalid port in configuration")
	}
	
	return nil
}

func Configuration_Port() int {
	return configuration.Port
}