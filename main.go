package main 

import (
	"net/http"
	"log"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
)

var configFile string

func init() {
	flag.StringVar(&configFile, "f", "/etc/kalibratorgosrv.json", "Server configuration file")
}

func RedirectHome(w http.ResponseWriter, r *http.Request) {
	log.Printf("redirecting to start page")
	http.Redirect(w, r, "/kalibratorgo/", http.StatusFound)
}

func main() {
	flag.Parse()
	
	if err := ReadSettings(configFile); err != nil {
		log.Fatalf("Failed to read/create config file %s (%s)", configFile, err.Error())
	}
	
	r := mux.NewRouter()
    r.HandleFunc("/", RedirectHome)
    
    content_sublouter := r.PathPrefix("/kalibratorgo").Subrouter()
    RegisterContent(content_sublouter)
    api_subrouter := r.PathPrefix("/api").Subrouter()
    RegisterApi(api_subrouter)
    
    //http.Handle("/", r)
    
    log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", Configuration_Port()), r))
} 

