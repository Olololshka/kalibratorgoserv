package main

import (
	"runtime"
	"path"
	"net/http"
)

func GetWorkDir() string {
	_, filename, _, _ := runtime.Caller(1)
	currentPath := path.Dir(filename)
	
	return string(http.Dir(currentPath))
}